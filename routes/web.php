<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

use App\Http\Controllers\PostController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/master', function (){
    return view('adminlte.master');
});

Route::get('/', function(){
    return view('items.index');
});

Route::get('/data-table', function(){
    return view('items.data-table');
});


// CRUD Laravel
Route::get('/pertanyaan/create', 'PostController@create');
Route::post('/pertanyaan', 'PostController@store');
Route::get('/pertanyaan', 'PostController@index');
Route::get('/pertanyaan/{id}', 'PostController@show');
Route::get('/pertanyaan/{id}/edit', 'PostController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PostController@update');
Route::delete('/pertanyaan/{id}', 'PostController@destroy');