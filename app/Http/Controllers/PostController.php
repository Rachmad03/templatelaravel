<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PostController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi'   => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi"   => $request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Tersimpan');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get(); // artinya sama dengan select * from pertanyaan
        // dd($pertanyaan);
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id){
        $tanya = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($tanya);
        return view('pertanyaan.show', compact('tanya'));
    }

    public function edit($id){
        $tanya = DB::table('pertanyaan')->where('id', $id)->first();

        return view('pertanyaan.edit', compact('tanya'));
    }

    public function update($id, Request $request){
        // $request->validate([
        //     'judul' => 'required|unique:pertanyaan',
        //     'isi'   => 'required'
        // ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi'   => $request['isi']
                    ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Di Ubah');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Di hapus');
    }
}
